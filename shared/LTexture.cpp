#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <string>

#include "PimpGlobals.h"
#include "LTexture.h"

LTexture::LTexture()
{
	mTexture = NULL;
	int mWidth = 0;
	int mHeight = 0;

}

LTexture::~LTexture()
{
	free();
}

bool LTexture::loadFromFile( std::string path )
{
	free();

	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
			
		SDL_SetColorKey( loadedSurface, SDL_TRUE, 
					SDL_MapRGB( loadedSurface->format, 
					KEYCOLOR.r, KEYCOLOR.g, KEYCOLOR.b ) );
		



		SDL_Surface* formattedSurface = SDL_ConvertSurface( loadedSurface, 
				SDL_GetWindowSurface( mWindow )->format, NULL );
		if( formattedSurface == NULL )
		{
			printf( "Unable to convert loaded surface to display format! SDL Error: %s\n", SDL_GetError() );
		}
		else
		{
			/*	
			newTexture = SDL_CreateTexture( mRenderer, SDL_GetWindowPixelFormat( mWindow ),
					SDL_TEXTUREACCESS_STREAMING, formattedSurface->w, formattedSurface->h );

			if( newTexture == NULL )
			{
				printf( "Unable to create a blank texture! SDL Error: %s\n", SDL_GetError() );
			}
			else
			{
				SDL_LockTexture( newTexture, NULL, &mPixels, &mPitch );

				memcpy( mPixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h );

				SDL_UnlockTexture( newTexture );
				mPixels = NULL;

				mWidth = formattedSurface->w;
				mHeight = formattedSurface->h;
			}
			*/

			
			newTexture = SDL_CreateTextureFromSurface( mRenderer, formattedSurface );

			mWidth = formattedSurface->w;
			mHeight = formattedSurface->h;

			SDL_FreeSurface( formattedSurface );
			
		}

		SDL_FreeSurface( loadedSurface );
	}

	mTexture = newTexture;
	return mTexture != NULL;

}

#ifdef _SDL_TTF_H
bool LTexture::loadFromRenderedText( std::string textureText, SDL_Color textColor, TTF_Font* lFont )
{
	free();

	SDL_Surface* textSurface = TTF_RenderText_Solid( lFont, textureText.c_str(), textColor );
	if( textSurface == NULL )
	{
		printf( "Unable to render text surface! SDL_TTF error: %s\n", TTF_GetError() );
	}
	else
	{
		mTexture = SDL_CreateTextureFromSurface( mRenderer, textSurface );
		if( mTexture == NULL )
		{
			printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
		}
		else
		{
			mWidth = textSurface->w;
			mHeight = textSurface->h;
		}

		SDL_FreeSurface( textSurface );
	}

	return mTexture != NULL;
}
#endif

void LTexture::free()
{
	if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

void LTexture::setColor( Uint8 red, Uint8 green, Uint8 blue )
{
	SDL_SetTextureColorMod( mTexture, red, green, blue );
}

void LTexture::setBlendMode( SDL_BlendMode blending )
{
	SDL_SetTextureBlendMode( mTexture, blending );
}

void LTexture::setAlpha( Uint8 alpha )
{
	SDL_SetTextureAlphaMod( mTexture, alpha );
}

void LTexture::render( int x, int y, SDL_Rect* clip, double angle,
			SDL_Point* center, SDL_RendererFlip flip )
{
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };

	if( clip != NULL )
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopyEx( mRenderer, mTexture, clip, &renderQuad, angle, center, flip );
}

bool LTexture::lockTexture()
{
	bool success = true;

	if( mPixels != NULL )
	{
		printf( "Texture is already locked!\n" );
		success = false;
	}
	else
	{
		if( SDL_LockTexture( mTexture, NULL, &mPixels, &mPitch ) != 0 )
		{
			printf( "Unable to lock texture! %s\n", SDL_GetError() );
			success = false;
		}
	}

	return success;
}

bool LTexture::unlockTexture()
{
	bool success = true;

	if( mPixels == NULL )
	{
		printf( "Texture is not locked!\n" );
		success = false;
	}
	else
	{
		SDL_UnlockTexture( mTexture );
		mPixels = NULL;
		mPitch = 0;
	}

	return success;
}



int LTexture::getWidth()
{
	return mWidth;
}

int LTexture::getHeight()
{
	return mHeight;
}

void* LTexture::getPixels()
{
	return mPixels;
}

int LTexture::getPitch()
{
	return mPitch;
}

void LTexture::setmRenderer( SDL_Renderer* rRenderer )
{
	mRenderer = rRenderer;
}

void LTexture::setmWindow( SDL_Window* rWindow )
{
	mWindow = rWindow;
}

SDL_Texture* LTexture::getmTexture()
{
	return mTexture;
}
