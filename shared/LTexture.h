#ifndef _LTEXTURE_H
#define _LTEXTURE_H

#include <SDL_ttf.h>
#include <string>

class LTexture
{
	public:
		LTexture();
		~LTexture();
		bool loadFromFile( std::string path );

		#ifdef _SDL_TTF_H
		bool loadFromRenderedText( std::string textureText, SDL_Color textColor, TTF_Font* lFont );
		#endif

		void free();

		void setColor( Uint8 red, Uint8 green, Uint8 blue );
		void setBlendMode( SDL_BlendMode blending );
		void setAlpha( Uint8 alpha );


		void render( int x, int y, SDL_Rect* clip = NULL, 
		double angle = 0.0, SDL_Point* center = NULL,
		SDL_RendererFlip flip = SDL_FLIP_NONE );
		int getWidth();
		int getHeight();
		void setmRenderer( SDL_Renderer* );
		void setmWindow( SDL_Window* );

		SDL_Texture* getmTexture();

		bool lockTexture();
		bool unlockTexture();
		void* getPixels();
		int getPitch();
	private:
		SDL_Texture* mTexture;
		void* mPixels;
		int mPitch;

		int mWidth;
		int mHeight;
		SDL_Renderer* mRenderer;
		SDL_Window* mWindow;
};
#endif
