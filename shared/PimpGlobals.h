#ifndef _PIMPGLOBALS_H
#define _PIMPGLOBALS_H

#include <SDL.h>

//Amber
const SDL_Color ACOLOR = { 0xFF, 0xC2, 0x00 };

//To make transparent
const SDL_Color KEYCOLOR = { 0x00, 0xFF, 0xEE };

const SDL_Color BGCOLOR = { 0x00, 0x00, 0x00 };

namespace PimpGlobals
{
	double abs( double N );
    int abs( int N );
}
#endif
