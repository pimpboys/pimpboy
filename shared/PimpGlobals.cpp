namespace PimpGlobals
{
	double abs( double N )
	{
		return ( N < 0.0 ? N * -1 : N );
	}

    int abs( int N )
    {
        return ( N < 0 ? N * -1 : N );
    }
}
