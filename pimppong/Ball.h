#ifndef _BALL_H
#define _BALL_H

#include <SDL.h>
#include <vector>
#include "Paddle.h"
#include "Scorekeeper.h"

class Ball
{
	public:
		static const int BALL_WIDTH = 32;
		static const int BALL_HEIGHT = 32;

		Ball();
		
		void move( Paddle wall[], int antallPad );
		void render();
		
		void init( Scorekeeper* scorer, SDL_Renderer* rRenderer, SDL_Window* rWindow );
		void reset();
		
		double mPosX, mPosY;
	
		double mVelX, mVelY;

	private:
		bool checkCollision( SDL_Rect& a, SDL_Rect& b );
		bool pxCollision( SDL_Rect& b );
		bool hitPadLast;

		double mVel;

		double mVinkler [11] = { 0.9, 0.8, 0.72, 0.65, 0.55, 0.5, 
			0.55, 0.65, 0.72, 0.8, 0.9 };

		SDL_Renderer* mRenderer;
		Scorekeeper* mScorePtr;
		std::vector<SDL_Rect> mColliders;
		SDL_Rect mCollider; 
		LTexture mTexture;
		void shiftColliders();
};
#endif
