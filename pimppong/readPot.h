#ifndef READPOT_H
#define READPOT_H

class readPot
{
public:
	readPot();
	void setUp();
	int getReadingOne();
	int getReadingTwo();

private:
	void discharge();
	int time_to_charge_AB();
	int time_to_charge_CD();
	
};

#endif
