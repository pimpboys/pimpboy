#include <wiringPi.h>
#include "readPot.h"
#include <chrono>
#include <thread>
#include <iostream>

#define A	17
#define B	22
#define C	1
#define D	2
#define E	13
#define F	19
#define G	3
#define H	4
readPot::readPot(){
}
void readPot::setUp()
{
	wiringPiSetupGpio();
	pinMode(A, OUTPUT);
	pinMode(E, OUTPUT);
}

/*void readPot::discharge()
{
	pinMode(B, OUTPUT);
	digitalWrite(B, LOW);
	std::this_thread::sleep_for(std::chrono::milliseconds(20));
}*/

int readPot::getReadingOne()
{
	//discharge();
	return time_to_charge_AB();
  /*      	
	int i;
	int tall = 0;
	for( i = 0; i < 1;i++ ){
		tall = tall + time_to_charge();
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	return tall/i;
*/	
}

int readPot::getReadingTwo()
{
	return time_to_charge_CD();
}

int readPot::time_to_charge_AB()
{
	pinMode(B,INPUT);
	int i = 0;
	digitalWrite(A, HIGH);
	int cond = 0;
	while(cond == 0)
	{
		i++;
		std::this_thread::sleep_for(std::chrono::nanoseconds(1));
		cond = digitalRead(B);
	}
	pinMode(B, OUTPUT);
	digitalWrite(B, LOW);
	return i;
}

int readPot::time_to_charge_CD()
{
	pinMode(F,INPUT);
	int i = 0;
	digitalWrite(E, HIGH);
	int cond = 0;
	while(cond == 0)
	{
		i++;
		std::this_thread::sleep_for(std::chrono::nanoseconds(1));
		cond = digitalRead(F);
	}
	pinMode(F, OUTPUT);
	digitalWrite(F, LOW);
	return i;
}

/*int main()
{
	readPot rad = readPot();
	rad.setUp();
	while( true )
	{
		std::cout << rad.getReading() << std::endl;
		std::cin.get();
		//std::this_thread::sleep_for(std::chrono::milliseconds(410));
	}
	return 0;
}*/
