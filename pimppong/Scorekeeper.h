#ifndef _SCOREKEEPER_H
#define _SCOREKEEPER_H

#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include <sstream>

#include "PimpGlobals.h"
#include "PongGlobals.h"
#include "LTexture.h"

class Scorekeeper
{
	public:
		Scorekeeper( int rGoal );
		int hasWinner();
		void render();
		void playerScored( int player );
		LTexture* getmTexture();
	private:
		LTexture mTexture;
		TTF_Font* mFont;
		std::stringstream mText;
		int player1;
		int player2;
		int goal;
};
#endif
