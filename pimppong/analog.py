
####################################
#         Analog Read Code         #
#         by Simon Monk            #
#         modified by Don Wilcher  #
#         Jan 1/31/15              #
####################################

# include RPi libraries in to Python code
import RPi.GPIO as GPIO
import time
import sys

# instantiate GPIO as an object
GPIO.setmode(GPIO.BCM)

# define GPIO pins with variables a_pin and b_pin
a_pin = 17
b_pin = 22

GPIO.setup(a_pin, GPIO.OUT)
# discharge function for reading capacitor data
def discharge():
    GPIO.setup(b_pin, GPIO.OUT)
    GPIO.output(b_pin, False)
    time.sleep(0.05)

# time function for capturing analog count value
def charge_time():
    GPIO.setup(b_pin, GPIO.IN)
    count = 0
    GPIO.output(a_pin, True)
    while not GPIO.input(b_pin):
        count = count +1
        time.sleep(0.0001)
    return count

# analog read function for reading charging and discharging data
def analog_read():
    discharge()
    return charge_time()

# a loop to display analog data count value on the screen
liste = []
while True:
    val = analog_read()
    if len(liste) < 5:
        liste.append(val)
    else:
        resultat = 0
        for tall in liste:
            resultat += tall
        resultat = resultat/10
        print resultat
        print ""
        liste = []
    time.sleep(0.001)
