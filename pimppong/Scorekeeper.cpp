#include <stdio.h>

#include "Scorekeeper.h"

Scorekeeper::Scorekeeper( int rGoal )
{
	mFont = NULL;
	mFont = TTF_OpenFont( "font/lazy.ttf", 28 );
	if( mFont == NULL )
	{
		printf( "Scorekeeper object couldn't open mFont.\n" );
	}

	player1 = 0;
	player2 = 0;
	goal = rGoal;

	mText.str( "" );
	mText << player1 << " - " << player2 << "\n";
}

int Scorekeeper::hasWinner()
{
	if( player1 >= goal )
	{
		return 1;
	}
	
	if( player2 >= goal )
	{
		return 2;
	}

	return 0;
}

void Scorekeeper::render()
{
	if( !mTexture.loadFromRenderedText( mText.str().c_str(), ACOLOR, mFont ) )
	{
		printf( "Seemingly can't render text because got bad return from loadFromRenderedText...\n" );
	}

	mTexture.render( ( SCREEN_WIDTH - mTexture.getWidth() ), ( SCREEN_HEIGHT - mTexture.getHeight() ) );
}

void Scorekeeper::playerScored( int player )
{
	if( player == 1 )
	{
		++player1;
	}
	else if( player == 2 )
	{
		++player2;
	}
	
//	printf( "Score: %d | %d\n", player1, player2 );
	mText.str( "" );
	mText << player1 << " - " << player2 << "\n";
}

LTexture* Scorekeeper::getmTexture()
{
	return &mTexture;
}
