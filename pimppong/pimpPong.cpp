//We use the SDL2, SDL2_image, stdio and string libraries.
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <SDL_ttf.h>
#include <iostream>
#include <unistd.h>


// Our own headers and such should go here for ease of organisation.
#include "PimpGlobals.h"
#include "PongGlobals.h"
#include "LTexture.h"
#include "Paddle.h"
#include "Scorekeeper.h"
#include "Ball.h"
#include "readPot.h"

// Forward function declarations go in order. 
bool init(); // Initializes SDL, gWindow, extension libraries and some settings.
bool loadMedia(char* path); // Loads all our media files. Images, sounds, etc.
void closeProgram(); // Deallocates resources and resets pointers.
void rendererHack(); // Unelegant way of making the LTexture class modular.
bool startProgram(); // Calls the functions necessary to prepare for main loop.
bool displaySimpleScreen();
int displayGoalScreen();
int gameLoop( int goal ); // This is the main loop. Keeps 'main' clean, and reduces indent.
bool twoPlayer = true;
readPot readpot;

SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;
TTF_Font* gFont = NULL;
std::stringstream gText;

bool gQuit;
LTexture gTextTexture, gBackgroundTexture;
// rendererHack() requires LTextureArray to work. Remember to update the const.
LTexture* LTextureArray[] = { &gTextTexture, &gBackgroundTexture };
const int LTEXTURE_TOTAL = 2;



// Returns true if everything went well. Sets up gWindow and gRenderer.
bool init()
{
	bool success = true;

	if( SDL_Init ( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL couldn't initialize. SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!\n" );
		}


		gWindow = SDL_CreateWindow( "Pimpin'", SDL_WINDOWPOS_UNDEFINED, 
					SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, 
					SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "gWindow couldn't be created. SDL_Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL )
			{
				printf( "Renderer couldn't be created! SDL_Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image couldn't initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}

				if( TTF_Init() == -1 )
				{
					printf( "SDL_ttf couldn't initialize! SDL_ttf Error: %s\n", TTF_GetError() );
					success = false;
				}

			}
		}
	}
	return success;
}


// Returns true if everything went well. Sets all media variables up.
// NOTE: Won't work if rendererHack() hasn't been called first.
bool loadMedia(char* path)
{
	std::cout << path << std::endl;
	bool success = true;
	char finalpath[200] = { NULL };
	std::cout << finalpath << std::endl;
	std::cout << "1" << std::endl;
	//getcwd(finalpath, 199);
	//std::cout << finalpath << std::endl;
	//std::cout << "2" << std::endl;
	strcat(finalpath, "/");
	std::cout << finalpath << std::endl;
	strcat(finalpath, path);
	std::cout << finalpath << std::endl;
	std::cout << "3" << std::endl;
	int len = sizeof(finalpath)/sizeof(char);
	int i;
	while(1)
	{
		if(finalpath[len--] == 'c')
		{
			break;
		}
	}
	std::string endpath = "font";
	for( i = 0; i < 5; i++)
	{
		finalpath[len - 2 + i] = endpath[i];
	std::cout << finalpath << std::endl;
	}
	strcat(finalpath, "/lazy.ttf");
	std::cout << finalpath << std::endl;
	gFont = TTF_OpenFont( finalpath, 64 );
	
	return success;
}


// Good to have. The OS does clean-up of its own, though... So no giant crisis.
void closeProgram()
{
	gTextTexture.free();

	TTF_CloseFont( gFont );
	gFont = NULL;

	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}


// My apologies in advance for this ugly solution.
// Improve the situation by removing the need for this kludge if you can.
void rendererHack()
{
	for( int i = 0; i < LTEXTURE_TOTAL; ++i )
	{
		LTextureArray[ i ]->setmRenderer( gRenderer );
		LTextureArray[ i ]->setmWindow( gWindow );
	} 
}


// Returns true if everything went well. Sets up everything for the main loop.
bool startProgram(char* path)
{
	bool success = true;
	readpot = readPot();
	readpot.setUp();

	if( !init() )
	{
	printf( "Failed to initialize!\n" );
		success = false;
	}
	else
	{
		rendererHack();

		if( !loadMedia(path) )
		{
			printf( "Failed to load media!\n" );
			success = false;
		}

	return success;
	}
}


// Obligatory main function. Returns 0 after a good run, -1 if exiting early.
int main( int argc, char *argv[] )
{

        if( !startProgram(argv[0]) )
        {
                printf( "Program couldn't start, abandoning...\n" );
                return -1;
        }

	gQuit = false;
	bool lQuit = false;
	int idle;
	while( !gQuit )
	{	int goal = 25;
		int winner = 0;

		if( !gQuit )
		{	
			lQuit = false;
			gText.str( "PLAY" );
		    	
		        idle = readpot.getReadingOne();
			while( !lQuit )
			{
				displaySimpleScreen();
                if( PimpGlobals::abs( readpot.getReadingOne() - idle) > 50 )
                {
                    lQuit = true;
                } 
			}
		}

		//SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );
		//SDL_RenderClear( gRenderer );
		
		/*if( !gQuit )
		{
			lQuit = false;

			while( !lQuit )
			{
				gText.str( "To how many points?" );
				lQuit = displaySimpleScreen();

				SDL_Event ev;

				while( SDL_PollEvent( &ev ) != 0 )
				{
					if( ev.type == SDL_KEYDOWN )
					{
						switch( ev.key.keysym.sym )
						{

							case SDLK_RIGHT:
								++goal;
								break;

							case SDLK_LEFT:
								--goal;
								break;
						}
					}
				}
				
	
				gText.str( "" );
				gText << goal;
				
				if( !gTextTexture.loadFromRenderedText( gText.str().c_str(),
					ACOLOR, gFont ) )
				{
					printf( ( "Can't render text in" 
						"pimpPong::displayWinningScreen because"
						 "bad return from" 
						 "loadFromRenderedText.\n" ) );
				}


				gTextTexture.render( ( SCREEN_WIDTH - 
					gTextTexture.getWidth() ) / 2,
					( ( SCREEN_HEIGHT - 
					gTextTexture.getHeight() )  / 2 ) +
					gTextTexture.getHeight() * 2 );

				SDL_RenderPresent( gRenderer );

			}

		}
		*/		
		gTextTexture.render( ( SCREEN_WIDTH - 
			gTextTexture.getWidth() ) / 2,
			( ( SCREEN_HEIGHT - 
			gTextTexture.getHeight() )  / 2 ) +
			gTextTexture.getHeight() * 2 );

		SDL_RenderPresent( gRenderer );


		if( !gQuit )
		{
			winner = gameLoop( goal );
		}

        idle = readpot.getReadingOne();
		if( !gQuit )
		{
			lQuit = false;

			if( winner == 1 )
			{
				gText.str( "Player one won!" );
			}
			else
			{
				gText.str( "Player one lost!" );
			}

			while( !lQuit )
			{
				lQuit =	displaySimpleScreen();
                if( PimpGlobals::abs( readpot.getReadingOne() - idle ) > 50 )
                {
                    lQuit = true;
                }
			}
		}

	}

        closeProgram();
        return 0;
}

bool displaySimpleScreen()
{
	bool quit = false;

	SDL_Event ev;
	while( SDL_PollEvent( &ev ) != 0 )
	{
		if( ev.type == SDL_KEYDOWN )
		{
			switch( ev.key.keysym.sym )
			{

				/*case SDLK_RETURN:
					quit = true;
					break;
				*/
				case SDLK_ESCAPE:
					quit = true;
					gQuit = true;
					break;
			}
		}
	}
	if( !gTextTexture.loadFromRenderedText( gText.str().c_str(),
				ACOLOR, gFont ) )
	{
		printf( ( "Can't render text in" 
				"pimpPong::displayWinningScreen because"
				 "bad return from" 
				 "loadFromRenderedText.\n" ) );
	}

	SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );
	SDL_RenderClear( gRenderer );

	gTextTexture.render( ( SCREEN_WIDTH/2 - 
				gTextTexture.getWidth()/2 )
				, ( SCREEN_HEIGHT/2 - 
				 gTextTexture.getHeight()/2 ) );

	SDL_RenderPresent( gRenderer );

	return quit;
}	


// This is the main loop. May, and likely will, end up calling
// Other looping functions.
int gameLoop( int goal )
{
	bool quit = false;
	SDL_Event ev;

	Paddle paddle1 ( 0, Paddle::PADDLEMODE_VERTICAL );
	Paddle paddle2 ( 0, Paddle::PADDLEMODE_HORIZONTAL );
	Ball balls[ 10 ];
	Scorekeeper scorer ( goal );
	scorer.getmTexture()->setmRenderer( gRenderer );

	int timer = 0;
	int timeToMove = 0;
	int timeToBall = 1;
	int numberOfBalls = 0;
	bool toDraw = true;

	while( !quit )
	{
		timer++;
		if( timer == timeToBall )
		{
			timer = 0;
			timeToBall = rand() % 120 + 120;
			
			balls[ numberOfBalls ] = Ball();
			balls[ numberOfBalls ].init( &scorer, gRenderer, gWindow );
			
			numberOfBalls++;
			if( numberOfBalls == sizeof(balls)/sizeof(Ball) )
			{
				timeToBall = -1;
			}
		}

		while( SDL_PollEvent( &ev ) != 0 )
		{
			if( ev.type == SDL_QUIT )
			{
				quit = true;
			}

			//paddle1.handleEvent( ev );
			//paddle2.handleEvent( ev );
		}
		if( twoPlayer )
		{
			//paddle1.move();
		}
		else
		{
			int x [ 10 ];
			int y [ 10 ];
			int velX [ 10 ];
			int velY [ 10 ];
			int ballWidth = balls[ 0 ].BALL_WIDTH;
			int ballHeight = balls[ 0 ].BALL_HEIGHT;
			for( int i = 0; i < numberOfBalls; i ++ )
			{
				x[ i ] = balls[ i ].mPosX + ballWidth / 2;
				y[ i ] = balls[ i ].mPosY + ballHeight / 2;	
				velX[ i ] = balls[ i ].mVelX;
				velY[ i ] = balls[ i ].mVelY;
			}	
			paddle1.aiMove( x, y, velX, velY, numberOfBalls );
		}
		timeToMove ++;
		if( timeToMove > 1 ){
			paddle2.mover(readpot.getReadingOne());
			paddle1.mover(readpot.getReadingTwo());
			timeToMove = 0;
		}
		//paddle2.move();
		Paddle padder [] = { paddle1, paddle2 };
		for( int i = 0; i < numberOfBalls; i++ )
		{
			balls[ i ].move(padder, 2);
		}


	//	if( toDraw )
	//{ 
		SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );
		SDL_RenderClear( gRenderer );

		SDL_SetRenderDrawColor( gRenderer, ACOLOR.r, ACOLOR.g, ACOLOR.b, 0xFF );
		SDL_RenderDrawLine( gRenderer, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );

		paddle1.render( gRenderer );
		paddle2.render( gRenderer );

		scorer.render();
		for( int i = 0; i < numberOfBalls; i++ )
		{
			balls[ i ].render();
		}

		SDL_RenderPresent( gRenderer );
	//	toDraw = false;
	//}
	//else
	//{
	//	toDraw = true;
	//}
		if( scorer.hasWinner() )
		{
			return scorer.hasWinner();
		}

	}
}
