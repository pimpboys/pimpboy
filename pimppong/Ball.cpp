//Using SDL, standard IO, and strings
#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <SDL2/SDL_image.h>
#include <vector>

#include "LTexture.h"
#include "Ball.h"
#include "Paddle.h"
#include "PongGlobals.h"

//Konstruktør
Ball::Ball()
{

	mCollider.w = BALL_WIDTH;
	mCollider.h = BALL_WIDTH;

	hitPadLast = false;

	mColliders.resize( 17 );

	mColliders[ 0 ].w = 8;
	mColliders[ 0 ].h = 1;

	mColliders[ 1 ].w = 12;
	mColliders[ 1 ].h = 1;

	mColliders[ 2 ].w = 16;
	mColliders[ 2 ].h = 1;

	mColliders[ 3 ].w = 18;
	mColliders[ 3 ].h = 1;

	mColliders[ 4 ].w = 22;
	mColliders[ 4 ].h = 2;

	mColliders[ 5 ].w = 24;
	mColliders[ 5 ].h = 1;

	mColliders[ 6 ].w = 26;
	mColliders[ 6 ].h = 2;

	mColliders[ 7 ].w = 28;
	mColliders[ 7 ].h = 2;

	mColliders[ 8 ].w = 30;
	mColliders[ 8 ].h = 8;

	mColliders[ 9 ].w = 28;
	mColliders[ 9 ].h = 2;

	mColliders[ 10 ].w = 26;
	mColliders[ 10 ].h = 2;

	mColliders[ 11 ].w = 24;
	mColliders[ 11 ].h = 1;

	mColliders[ 12 ].w = 22;
	mColliders[ 12 ].h = 2;

	mColliders[ 13 ].w = 18;
	mColliders[ 13 ].h = 1;

	mColliders[ 14 ].w = 16;
	mColliders[ 14 ].h = 1;

	mColliders[ 15 ].w = 12;
	mColliders[ 15 ].h = 1;

	mColliders[ 16 ].w = 8;
	mColliders[ 16 ].h = 1;

	reset();
} 

void Ball::render()
{
	//mTexture.render( static_cast<int>( mPosX ), static_cast<int>( mPosY ) );
	for( int i = 0; i < mColliders.size(); i++ )
	{
		SDL_RenderFillRect( mRenderer, &mColliders[i] );
	}
}

//Sjekker om a har kollidert med b.
bool Ball::checkCollision( SDL_Rect& a, SDL_Rect& b )
{
	int leftA, leftB, rightA, rightB, downA, downB, topA, topB;

	//Regner ut hvor sidene er
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	downA = a.y+a.h;

	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	downB = b.y+b.h;
	
	//Sjekker om den har kollidert.
	//X-verdiene må overlappe, og det må Y-verdiene også.
	//Hvis en eller begge av disse ikke stemmer, kolliderer den ikke.
	if( downA <= topB )
	{
		return false;
	}

	if( topA >= downB )
	{
		return false;
	}

	if( rightA <= leftB )
	{
		return false;
	}

	if( leftA >= rightB )
	{
		return false;
	}

	return true;
}

bool Ball::pxCollision( SDL_Rect& b )
{
	for( int i = 0; i < mColliders.size(); i++ )
	{
		if( checkCollision( mColliders[ i ], b ) )
		{
			return true;
		}

	}
	return false;
}

//Flytter ballen i forhold til hastigheten den har. 
void Ball::move( Paddle wall[], int antallPad )
{	
	if( mPosX != mPosX || mPosY != mPosY || mVelX != mVelX || mVelY != mVelY )
	{
		reset();
	}

	//Flyttes i x-akse
	mPosX += mVelX;

	//Flytter kollideringsboksen
	mCollider.x = mPosX;
	
	//Flyttes i y-akse
	mPosY += mVelY;

	//Flytter kollideringsboksen
	mCollider.y = mPosY;

	// Checks if it has hit the centre line around which to change direction.
	double XdivY = static_cast<float>(mPosX) / static_cast<float>(mPosY);
	double WdivH = static_cast<float>(SCREEN_WIDTH) / static_cast<float>(SCREEN_HEIGHT);
	double diff = XdivY - WdivH;
	
	bool atLowerLeft = diff < 0;

	if( diff < 0 )
	{
		diff = -1*diff;
	}
	double epsilon = 0.08;
	bool isItIn = diff < epsilon;

	if( isItIn && hitPadLast )
	{
		hitPadLast = false;
		double oldPyt = pow( ( mVelX*mVelX ) + pow( mVelY, 2 ), 0.5); 
		std::swap( mVelX, mVelY );
		
		mVelY = -1*mVelY;
		
		if( mVelX > mVelY )
		{
			mVelX = -1*( mVelX + ( mVelX - mVelY ) );
		}
		else
		{
			mVelX = -1*( mVelX + ( mVelY - mVelX ) );
		}
		double newPyt = pow( ( mVelX*mVelX ) + pow( mVelY, 2 ), 0.5); 
		
		mVelX = (mVelX/newPyt)*oldPyt;

	
	}

	//Hvis ballen gikk out of bounds, eller kolliderte med noe,
	//skal den gå tilbake igjen.
	if( mPosX + BALL_WIDTH > SCREEN_WIDTH )
	{
		//Flytter ballen tilbake	
		mPosX -= mVelX;
		mCollider.x = mPosX;
		//Finner ut hva den kolliderte i,
		//og hvordan den eventuelt skal endre hastighet

		//Om den har kollidert med veggen på andre siden av padden.
		mVelX = -1*mVelX;
		//Om den har kommet seg bak padden.
	}
	

	
	//Hvis ballen gikk out of bounds, eller kolliderte med noe,
	//skal den gå tilbake igjen.
	if( mPosY + BALL_HEIGHT > SCREEN_HEIGHT )
	{
		//Flytter ballen tilbake	
		mPosY -= mVelY;
		mCollider.y = mPosY;
		
		mVelY = -1*mVelY;
	}

	shiftColliders();

	for( int i = 0; i < antallPad; i ++)
	{
		if( Ball::checkCollision( mCollider, wall[i].getRectPtr() ) &&
			Ball::pxCollision( wall[i].getRectPtr() ) )
		{
			hitPadLast = true;

			if( wall[i].getPaddleMode() == Paddle::PADDLEMODE_VERTICAL )
			{
				int ballMidt = ( mPosY + ( BALL_WIDTH / 2 ) );
				int punkt = ballMidt - wall[i].getRectPtr().y;
				int rute = 11 * punkt / wall[i].getRectPtr().h;

				rute = rute < 0 ? 0 : rute;
				int yDir = mVelY < 0 ? -1 : 1;

				float yPercentage = mVinkler[ rute ];
				float xPercentage = 1 - PimpGlobals::abs( mVinkler[ rute ] );
				
				mVelY = sqrt( pow( mVel, 2.0 ) *  yPercentage );

				if( rute <= 3 )
				{
					mVelY = mVelY * -1;
				}
				else if( rute <= 6 )
				{
					mVelY = mVelY * yDir;
				}
	
				mVelX = sqrt( pow( mVel, 2.0 ) *  xPercentage );
			}
			else
			{
				int ballMidt = ( mPosX + ( BALL_WIDTH / 2 ) );
				int punkt = ballMidt - wall[i].getRectPtr().x;
				int rute = 11 * punkt / wall[i].getRectPtr().w;

				rute = rute < 0 ? 0 : rute;
				int xDir = mVelX < 0 ? -1 : 1;

				float xPercentage = mVinkler[ rute ];
				float yPercentage = 1 - PimpGlobals::abs( mVinkler[ rute ] );
				

				mVelX = sqrt( pow( mVel, 2.0 ) *  xPercentage );

				if( rute <= 3 )
				{
					mVelX = mVelX * -1;
				}
				else if( rute <= 6 )
				{
					mVelX = mVelX * xDir;
				}
				mVelY = sqrt( pow( mVel, 2.0 ) *  yPercentage );
			}

			double mVelPlus = rand() % 3;

			mVel += mVelPlus / 10;
		}
	}

	if( mPosX < -1*BALL_WIDTH )
	{
		mScorePtr->playerScored( 2 );
		reset();
	}
	if( mPosY < -1*BALL_HEIGHT )
	{
		mScorePtr->playerScored( 1 );
		reset();
	}

}

void Ball::init( Scorekeeper* scorer, SDL_Renderer* rRenderer, SDL_Window* rWindow )
{
	mRenderer = rRenderer;
	mTexture.setmRenderer( rRenderer );
	mTexture.setmWindow( rWindow );

	mScorePtr = scorer;
	
	if( !mTexture.loadFromFile("img/ball.png") )
	{
		printf("Couldn't load ball.png!!!\n");
	}
	else
	{

		mTexture.setBlendMode( SDL_BLENDMODE_BLEND );
		/*
		if( !mTexture.lockTexture() )
		{
			printf( "Unable to lock ball texture!\n");
		}
		else
		{

			mTexture.setBlendMode( SDL_BLENDMODE_BLEND );


			Uint32* pixels = (Uint32*)mTexture.getPixels();
			int pixelCount = (mTexture.getPitch() / 4 ) * mTexture.getHeight();

			Uint32 colorKey = SDL_MapRGB( SDL_GetWindowSurface( rWindow )->format, 
					KEYCOLOR.r, KEYCOLOR.g, KEYCOLOR.b );
			Uint32 transparent = SDL_MapRGBA( SDL_GetWindowSurface( rWindow )->format,
					0x00, 0x00, 0x00, 0x00 );

			for( int i = 0; i < pixelCount; ++i )
			{
				if( pixels[ i ] == colorKey )
				{
					pixels[ i ] = transparent;
				}
			}

			mTexture.unlockTexture();
			SDL_RenderCopy( rRenderer, mTexture.getmTexture(), NULL, NULL );
		}
		*/

	}
}

void Ball::shiftColliders()
{
	int currY = mCollider.y;
	for( int i = 0; i < mColliders.size(); i++ )
	{
		mColliders[ i ].x = mCollider.x + 
			( mCollider.w - mColliders[ i ].w / 2 );
		mColliders[ i ].y = currY;
		currY += mColliders[ i ].h;
	}
}

void Ball::reset()
{
	mPosX = SCREEN_WIDTH / 2;
	mPosY = SCREEN_HEIGHT / 2;

	mVel = ( rand() % 6 ) + 3;

	float xPercentage = ( rand() % 100 );
	float yPercentage = ( 100 - xPercentage );

	xPercentage = xPercentage / 100;
	yPercentage = yPercentage / 100;

	mVelX = sqrt( pow( mVel, 2.0 ) * ( 1 * xPercentage ) );
	mVelY = sqrt( pow( mVel, 2.0 ) * ( 1 * yPercentage ) );

	if( rand() % 2 == 0 )
	{
		mVelX = -1*mVelX;
	}
	if( rand() % 2 == 1 )
	{
		mVelY = -1*mVelY;
	}


}
