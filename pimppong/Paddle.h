#ifndef _PADDLE_H
#define _PADDLE_H

#include <SDL.h>
#include <string>

class Paddle
{
	public:
		// enumerator siden en bitmaske ville vært overkill.
		enum PaddleMode
		{
			PADDLEMODE_VERTICAL,
			PADDLEMODE_HORIZONTAL
		};

		// Midlertidlig konstant: må byttes ut med utregning fra window for fair play.
		const int PADDLE_VEL = 10;

		Paddle( int offset, PaddleMode PADDLEMODE );
		void handleEvent( SDL_Event& ev );	
		void move();
		void render( SDL_Renderer* rRenderer );
		SDL_Rect& getRectPtr();
		PaddleMode getPaddleMode();
		void aiMove( int x[ 10 ], int y[ 10 ], int velX[ 10 ],
			       	int velY[ 10 ], int& numberOfBalls );
		void mover( int c );

	private:
		PaddleMode mPaddleMode;
		SDL_Rect mRect;
		int mVelX, mVelY;
		
};

#endif
