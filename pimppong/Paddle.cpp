#include <SDL.h>
#include <stdio.h>

#include "PongGlobals.h"
#include "PimpGlobals.h"
#include "Paddle.h"
#include "Ball.h"
#include <string>
#include <iostream>

int ballToFollow = -1;

Paddle::Paddle( int offset, PaddleMode PADDLEMODE )
{
	mVelX = 0;
	mVelY = 0;
	mPaddleMode = PADDLEMODE;

	// mRect verdiene her burde forandres så de er litt mer softkoda.
	if( mPaddleMode == PADDLEMODE_VERTICAL )
	{
		mRect = { offset, 180, 15, 60 };
	}
	else if( mPaddleMode == PADDLEMODE_HORIZONTAL )
	{
		mRect = { 180, offset, 60, 15 };
	}
}



void Paddle::mover( int i )
{
	/*if( c == 'o' )
	{
		mVelY -= PADDLE_VEL;
	}
	else if( c == 'p')
	{
		mVelY += PADDLE_VEL;
	}*/
	if(mPaddleMode == PADDLEMODE_VERTICAL){
	int delta = 15;
	int factor = 5;
	if( ( mRect.y + delta ) > i*factor and ( mRect.y - delta ) < i*factor )
	{
		return;
	}
	mRect.y = i*factor;

	if( mRect.y < 0 || mRect.y > SCREEN_WIDTH - mRect.h )
	{
		mRect.y = SCREEN_HEIGHT - mRect.h;
	}
	}
	else{
	int delta = 15;
	int factor = 4;
	if( ( mRect.x + delta ) > i*factor and ( mRect.x - delta ) < i*factor )
	{
		return;
	}
	mRect.x = i*factor;

	if( mRect.x < 0 || mRect.x > SCREEN_WIDTH - mRect.w )
	{
		mRect.x = SCREEN_WIDTH - mRect.w;
	}
	}
}



void Paddle::handleEvent( SDL_Event& ev )
{
	if( mPaddleMode == PADDLEMODE_VERTICAL )
	{
		if( ev.type == SDL_KEYDOWN && ev.key.repeat == 0 )
		{
			switch( ev.key.keysym.sym )
			{
				case SDLK_q:
					mVelY -= PADDLE_VEL;
					break;

				case SDLK_a:
					mVelY += PADDLE_VEL;
					break;
			}
		}
		else if( ev.type == SDL_KEYUP && ev.key.repeat == 0 )
		{
			switch( ev.key.keysym.sym )
			{
				case SDLK_q:
					mVelY += PADDLE_VEL;
					break;

				case SDLK_a:
					mVelY -= PADDLE_VEL;
					break;
			}
		}
	}
	else if( mPaddleMode == PADDLEMODE_HORIZONTAL )
	{
		if( ev.type == SDL_KEYDOWN && ev.key.repeat == 0 )
		{
			switch( ev.key.keysym.sym )
			{
				case SDLK_o:
					mVelX -= PADDLE_VEL;
					break;

				case SDLK_p:
					mVelX += PADDLE_VEL;
					break;
			}
		}
		else if( ev.type == SDL_KEYUP && ev.key.repeat == 0 )
		{
			switch( ev.key.keysym.sym )
			{
				case SDLK_o:
					mVelX += PADDLE_VEL;
					break;

				case SDLK_p:
					mVelX -= PADDLE_VEL;
					break;
			}
		}
	}
}

void Paddle::move()
{
	if( mPaddleMode == PADDLEMODE_VERTICAL )
	{
		mRect.y += mVelY;

		if( mRect.y < 0 || mRect.y > SCREEN_HEIGHT - mRect.h )
		{
			mRect.y -= mVelY;
		}
	}
	else if (mPaddleMode == PADDLEMODE_HORIZONTAL )
	{
		mRect.x += mVelX;

		if( mRect.x < 0 || mRect.x > SCREEN_WIDTH - mRect.w )
		{
			mRect.x -= mVelX;
		}
	}
}

//ai som bestemmer seg for en ball, og jakter etter den til den treffer den.
//Kansje det gjør den litt raskere.
/*void Paddle::aiMove( int x[ 10 ], int y[ 10 ], int velX[ 10 ], int velY[ 10 ]
		, int& numberOfBalls )
{
	if( ballToFollow == -1 ) {
		int candBall = 1;
		int minVal = SCREEN_WIDTH;
		for( int i = 0; i < numberOfBalls; i++ )
		{
			if( velX[ i ] >= 0 )
			{
				continue;
			}
			if( velY[ i ] < 0 &&
				y[ i ] + velY[ i ] * x[ i ] / -1*velX[ i ] < 0 )
			{
				continue;
			}
			if( -1*( x[ i ] / velX[ i ] ) < minVal )
			{
				candBall = i;
				minVal = -1*( x[ i ] / velX[ i ] );
			}
		}
		ballToFollow = candBall;
	}
	if( y[ ballToFollow ] < mRect.y + ( mRect.h / 2 ) )
	{
		mVelY = -1*PADDLE_VEL;
	}
	else
	{
		mVelY = PADDLE_VEL;
	}
	if( velX[ ballToFollow ] > 0 ) {
		ballToFollow = -1;
	}
	Paddle::move();
}*/

//Ai som revurderer hvilken ball den skal jakte hele tiden. Den forsøker 
//fremdeles baller som den ikke rekker.
/*void Paddle::aiMove( int x[ 10 ], int y[ 10 ], int velX[ 10 ], int velY[ 10 ]
		, int& numberOfBalls )
{
	int candBall = 1;
	int minVal = SCREEN_WIDTH;
	for( int i = 0; i < numberOfBalls; i++ )
	{
		if( velX[ i ] >= 0 )
		{
			continue;
		}
		if( velY[ i ] < 0 &&
			y[ i ] + velY[ i ] * x[ i ] / -1*velX[ i ] < 0 )
		{
			continue;
		}
		if( -1*( x[ i ] / velX[ i ] ) < minVal )
		{
			candBall = i;
			minVal = -1*( x[ i ] / velX[ i ] );
		}
	}
	ballToFollow = candBall;
	if( y[ ballToFollow ] < mRect.y + ( mRect.h / 2 ) )
	{
		mVelY = -1*PADDLE_VEL;
	}
	else
	{
		mVelY = PADDLE_VEL;
	}
	if( velX[ ballToFollow ] > 0 ) {
		ballToFollow = -1;
	}
	Paddle::move();
}*/

void Paddle::aiMove( int x[ 10 ], int y[ 10 ], int velX[ 10 ], int velY[ 10 ]
		, int& numberOfBalls )
{
	int candBall = 1;
	int minVal = SCREEN_WIDTH;
	for( int i = 0; i < numberOfBalls; i++ )
	{
		if( velX[ i ] >= 0 )
		{
			continue;
		}
		if( velY[ i ] < 0 &&
			y[ i ] + velY[ i ] * x[ i ] / -1*velX[ i ] < 0 )
		{
			continue;
		}
		if( velY [ i ] < 0 &&
			x[ i ] != 0 &&
			y[ i ] + velY[ i ] * x[ i ] / -1*velX[ i ] - mRect.y
			> PADDLE_VEL / ( x[ i ] / -1*velX[ i ] ) - 16 )
		{
			continue;
		}
		if( -1*( x[ i ] / velX[ i ] ) < minVal )
		{
			candBall = i;
			minVal = -1*( x[ i ] / velX[ i ] );
		}
	}
	ballToFollow = candBall;
	int delta = 5;
	if( y[ballToFollow] + delta > mRect.y &&
			y[ballToFollow] - delta < mRect.y ){
		mVelY = 0;
	}
	else if( y[ ballToFollow ] < mRect.y + ( mRect.h / 2 ) )
	{
		mVelY = -1*PADDLE_VEL;
	}
	else
	{
		mVelY = PADDLE_VEL;
	}
	if( velX[ ballToFollow ] > 0 ) {
		ballToFollow = -1;
	}
	Paddle::move();
}

void Paddle::render( SDL_Renderer* rRenderer )
{
	SDL_RenderFillRect( rRenderer, &mRect );
}

SDL_Rect& Paddle::getRectPtr()
{
	return mRect;
}

Paddle::PaddleMode Paddle::getPaddleMode()
{
	return mPaddleMode;
}
